FROM python:3.6.0-alpine

WORKDIR /app
COPY requirements.txt /app

RUN apk --no-cache --update add  \
    build-base=0.4-r1  && \
    pip --no-cache-dir install -r /app/requirements.txt && \
    apk del build-base
COPY . /app
ENV  PYTHONUNBUFFERED=1

# ENTRYPOINT ["python3", "post_app.py"]
